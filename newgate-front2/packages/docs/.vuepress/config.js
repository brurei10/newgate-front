const sidebar = require('./sidebar');

module.exports = {
    dest: 'dist',
    locales: {
        '/': {
            title: 'Documentação Orsegups',
            description: 'Documentação do time orsegups para o Gateway e o Portal do cliente'
        }
    },
    themeConfig: {
        repo: 'https://gitlab.com/incuca/clientes/orsegups',
        editLinks: true,
        nav: [
            {
                text: 'Home',
                link: '/'
            }
        ],
        sidebar,
        sidebarDepth: 4,
    }
};
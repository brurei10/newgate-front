const generateSidebar = require('./generateSidebar');

/* Sidebars customizadas */
const gateway = generateSidebar({
    forPath: 'packages/gateway',
});

const portalCliente = generateSidebar({
    forPath: 'packages/portal-cliente',
});

module.exports = {
    /* API's */
    ...gateway,
    ...portalCliente,

    /* Sidebar Inicial */
    '/': [
        '/', // README.md
        {
            title: 'Pacotes',
            collapsable: false,
            children: [
                '/packages/gateway/',
                '/packages/portal-cliente/',
            ]
        },
    ],
}
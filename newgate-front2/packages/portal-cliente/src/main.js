import Vue from 'vue';
import Vuex from 'vuex';
import feathersVuex from 'feathers-vuex';
import LogRocket from 'logrocket';
import createLogPlugin from 'logrocket-vuex';

import feathersClient from './lib/feathersClient';
import createSession from './lib/session';
import App from './App.vue';
import router from './router';
import setupStore from './store';
import vuetify from './plugins/vuetify';
import './plugins/rules';
import './plugins/dateTime';
import './plugins/portal-vue';

const { VUE_APP_GATEWAY_URI, VUE_APP_LOGROCKET } = process.env;

Vue.config.productionTip = false;

let log;

if (VUE_APP_LOGROCKET) {
  LogRocket.init(VUE_APP_LOGROCKET);
  log = createLogPlugin(LogRocket);
}


Vue.use(Vuex);

const client = feathersClient(VUE_APP_GATEWAY_URI);
const { service, FeathersVuex } = feathersVuex(client, { idField: 'id' });
Vue.use(FeathersVuex);

const session = createSession(router, client);

const store = new Vuex.Store(setupStore({
  client, service, session, log,
}));

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');

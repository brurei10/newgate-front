import notifyModule from './notify.module';

export default function notify(store) {
  store.$notify = (textOrErr, type = 'error', icon) => {
    const text = textOrErr.message ? textOrErr.message : textOrErr;
    store.dispatch('notify/push', {
      text,
      type,
      icon,
    }, { root: true });
  };
  store.registerModule('notify', notifyModule);
}

import uniqid from 'uniqid';

export default {
  namespaced: true,
  state: {
    notifications: [],
  },
  actions: {
    push({ commit }, data) {
      const id = uniqid();
      const timeout = 5000;
      commit('addNotification', { id, timeout, ...data });
      setTimeout(
        () => commit('removeNotification', id),
        timeout,
      );
    },
    onNotificationClose({ commit }, id) {
      commit('removeNotification', id);
    },
  },
  mutations: {
    addNotification(state, notification) {
      state.notifications.push(notification);
    },
    removeNotification(state, notification) {
      state.notifications = state.notifications.filter(
        n => n.id !== notification.id,
      );
    },
  },
  getters: {
    notifications(state) {
      return state.notifications;
    },
  },
};

export function addAccessToken(store) {
  return (context) => {
    if (store.getters['session/loggedIn']) {
      context.params.query = context.params.query || {};
      const { accessToken } = store.state.session.token;
      context.params.query.accessToken = accessToken;
    }
  };
}

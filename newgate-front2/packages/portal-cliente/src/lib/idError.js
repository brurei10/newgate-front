export function isIdError(err, idError) {
  if (err.name === 'orsegups-id-error') {
    return err.idError.error === idError;
  }
  return false;
}

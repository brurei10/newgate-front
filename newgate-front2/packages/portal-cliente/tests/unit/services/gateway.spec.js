import axios from 'axios';
import Gateway from '@/services/gateway';

jest.mock('axios');

describe('gateway service', () => {
  it('exports an object', () => {
    expect(Gateway).toBeDefined();
  });

  it('get gateway api root', async () => {
    const url = process.env.VUE_APP_GATEWAY_URI;
    const result = {};
    axios.get.mockResolvedValue(result);
    const res = await Gateway.get();
    expect(res).toEqual(result);
    expect(axios.get).toHaveBeenCalledWith(url);
  });
});

import { addAccessToken } from '@/lib/session/session.hooks';

describe('session hooks', () => {
  describe('addAccessToken hook', () => {
    it('set accessToken if user is looged in', () => {
      const context = {
        params: {},
      };
      const store = {
        state: {
          session: {
            token: {
              accessToken: 'foo',
            },
          },
        },
        getters: {
          'session/loggedIn': true,
        },
      };
      const hook = addAccessToken(store);
      hook(context);
      expect(context.params.query.accessToken).toEqual('foo');
    });

    it('does nothing if user is not looged in', () => {
      const context = {
        params: {
          query: {},
        },
      };
      const store = {
        state: {
          session: {
            token: {
              accessToken: 'foo',
            },
          },
        },
        getters: {
          'session/loggedIn': false,
        },
      };
      const hook = addAccessToken(store);
      hook(context);
      expect(context.params.query.accessToken).not.toEqual('foo');
    });
  });
});

/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.changeColumn('Contracts', 'isActive', {
    type: Sequelize.BOOLEAN,
    allowNull: true,
  }),

  down: (QI, Sequelize) => QI.changeColumn('Contracts', 'isActive', {
    type: Sequelize.BOOLEAN,
    allowNull: false,
  }),
};

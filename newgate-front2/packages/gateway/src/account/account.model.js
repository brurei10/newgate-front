import Sequelize from 'sequelize';

/**
 * Account Model
 *
 * @name AccountModel
 * @memberof module:Account
 * @param {Feathers} app
 * @returns {Model}
 */
export default function AccountModel(app) {
  /**
   * @typedef {module:Token.AccountModel}
   * @property {Sequelize.INTEGER} id
   * @property {Sequelize.TEXT} contractCode
   * @property {Sequelize.STRING} empresaContrato
   * @property {Sequelize.STRING} filialContrato
   * @property {Sequelize.BOOLEAN} isActive
   * @property {Sequelize.BIGINT} userOwnerId
   * @property {Sequelize.STRING} cpfCnpjOwner
   * @property {Sequelize.STRING} identifier
   * @property {Sequelize.JSON} guests Lista de usuários convidados
   * @property {Sequelize.STRING} productName
   * @property {Sequelize.STRING} name
   */
  const Account = app.db.define('Account', {
    contractCode: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    empresaContrato: {
      type: Sequelize.STRING,
    },
    filialContrato: {
      type: Sequelize.STRING,
    },
    isActive: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
    },
    cpfCnpjOwner: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    identifier: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    guests: {
      type: Sequelize.JSON,
      defaultValue: [],
    },
    productName: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
  }, {
    timestamps: false,
  });


  Account.associate = (models) => {
    Account.belongsTo(models.User, {
      foreignKey: 'userOwnerId',
    });
  };

  return Account;
}

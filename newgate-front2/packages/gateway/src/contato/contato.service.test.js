import tipoAll from './__fixtures__/tipoAll';
import createContato from './contato.service';
import app from '../app';

describe('contato service', () => {
  it('get portalmobile api', async () => {
    const Contato = createContato(app);
    const { data } = await Contato.find();
    expect(data).toEqual(tipoAll);
  });
});

import axios from 'axios';

/**
 * Creates Contato service
 *
 * @export
 * @param {Featherjs} app
 * @returns Contato
 */
export default function createContato(app) {
  return {
    find() {
      const url = `${app.get('portalMobile').uri}/portalcliente/v1/contato/tipo/all`;
      return axios({
        method: 'get',
        responseType: 'json',
        url,
      });
    },
  };
}

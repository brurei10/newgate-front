import axios from 'axios';
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import loadOrsIdToken from './loadOrsIdToken';

jest.mock('axios');

describe('loadOrsIdToken', () => {
  let app;
  let idConfig;

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
    idConfig = app.get('orsegupsId');
    axios.post.mockReset();
  });

  it('attach in context token and user from orsegups id', async () => {
    const orsegupsID = { access_token: 'foo', expires_in: 900, user: {} };
    axios.post = jest.fn(() => Promise.resolve({ data: orsegupsID }));
    const res = await loadOrsIdToken({
      app,
      result: null,
      params: {
        query: {
          username: 'foo',
          password: 'bar',
        },
      },
    });
    const params = 'grant_type=password&username=foo?product=2&password=bar';
    expect(axios.post).toBeCalledWith(
      `${idConfig.uri}/oauth/token?${params}`,
      expect.any(Object),
      expect.objectContaining({
        auth: {
          username: idConfig.username,
          password: idConfig.password,
        },
      }),
    );
    expect(res).toMatchObject({ orsegupsID });
  });

  it('dont attach token if result was found', async () => {
    const context = {
      app,
      result: {},
    };
    const res = await loadOrsIdToken(context);
    expect(axios.get).not.toBeCalled();
    expect(res).toBe(context);
  });
});

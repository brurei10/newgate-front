import feathersSequelize from 'feathers-sequelize';
/**
 * Token Service
 *
 * @name TokenService
 * @memberof Token
 * @param {Object} settings
 * @returns {Service}
 */
export default function TokenService(settings) {
  return feathersSequelize(settings);
}

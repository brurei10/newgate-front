/* istanbul ignore file */
import axios from 'axios';
import { logger } from '@/logger';

/**
 * Load accounts from orsegups ID if orsegupsID is found in context
 *
 * @export
 * @memberof module:TokenHooks
 * @typedef module:TokenHooks.loadOrsIdAccounts
 * @name loadOrsIdAccounts
 * @param {*} context
 * @returns {Promise.<Context>} hook context with orsegupsID property
 */
export default function loadOrsIdAccounts(context) {
  const { app, params } = context;
  if (context.orsegupsID) {
    // console.log('ors id', context.orsegupsID);
    // if orsegupsID was set by loadOrsIdToken hook
    // we also have access to username and password
    logger.info('loading ID accounts for %s', params.query.username);
    const orsegupsId = app.get('orsegupsId');
    const idEndpoint = orsegupsId.uri;
    const append = 'product=2';
    const config = {
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `bearer ${context.orsegupsID.access_token}`,
      },
    };
    return axios
      .get(`${idEndpoint}/account/all?${append}`, config)
      .then((res) => {
        logger.debug('loaded accounts', res.data.data);
        return {
          ...context,
          orsegupsID: {
            ...context.orsegupsID,
            accounts: res.data.data,
          },
        };
      });
  }

  logger.info('skipping loadOrsIdAccounts');
  return Promise.resolve(context);
}

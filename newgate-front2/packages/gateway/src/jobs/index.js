import { logger } from '@/logger';

export default function setupJobs(app) {
  app.jobs = {
    register(jobName, jobPromise) {
      const log = msg => `job:${jobName} ${msg}`;
      logger.debug(log('registered'));

      app.use((req, res, next) => {
        if (req.path === `/jobs/${jobName}`) {
          logger.debug(log('started'));
          // return needed only for testing
          return jobPromise()
            .then(() => {
              logger.debug(log('sending 200'));
              res.status(200);
            })
            .catch((err) => {
              logger.error(log('sending 500'), err);
              res.status(500);
            })
            .finally(() => {
              logger.debug(log('finished'));
              res.send();
            });
        }
        next();
        return Promise.resolve();
      });
    },
  };
}

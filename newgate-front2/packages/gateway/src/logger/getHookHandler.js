// import { inspect } from 'util';
import { cloneDeep } from 'lodash';
import logger from './winstonLogger';

// To see more detailed messages, uncomment the following line
if (process.env.DEBUG_LEVEL) logger.level = process.env.DEBUG_LEVEL;

/**
 * Cria um hook para logar ações no Gateway
 */
export default function getHookHandler() {
  return (context) => {
    // This debugs the service call and a stringified version of the hook context
    // You can customize the mssage (and logger) to your needs
    logger.debug(`${context.type} app.service('${context.path}').${context.method}()`);

    if (typeof context.toJSON === 'function') {
      const revisedContext = cloneDeep(context);
      // strip accessToken
      if (context.params && context.params.query && context.params.query.accessToken) {
        revisedContext.params.query.accessToken = `${revisedContext.params.query.accessToken.slice(0, 10)}...`;
      }
      // logger.debug('Hook Context %s', inspect(revisedContext));
    }

    if (context.error) {
      logger.error(context.error);
    }
  };
}

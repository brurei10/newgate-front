import cors from 'cors';
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

import setupCors from './cors';

jest.mock('cors');

describe('cors', () => {
  let app;

  beforeAll(() => {
    jest.setTimeout(30000);
  });

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
  });

  it('sets a middleware on app', () => {
    const mock = jest.fn();
    app.use = jest.fn();
    cors.mockImplementation(() => mock);
    app.configure(setupCors);
    expect(app.use).toBeCalledWith(mock);
  });

  it('blocks not whitelisted', (done) => {
    expect.assertions(2);
    app.use = jest.fn();
    cors.mockImplementation((settings) => {
      const origin = 'http://notWhitelistedHost';
      const callback = jest.fn((err) => {
        expect(err).toBeInstanceOf(Error);
        expect(err.message).toMatch(origin);
        done();
      });
      settings.origin(origin, callback);
    });
    app.configure(setupCors);
  });
});

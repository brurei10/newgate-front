/* eslint-disable camelcase */
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import axios from 'axios';
import PasswordChangeService from './passwordChange.service';

jest.mock('axios');

describe('passwordChange.service create', () => {
  let app;

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
    app.serverToken = {
      access_token: 'foo',
    };
  });

  it('send orsegups changepassword request and return result', async () => {
    const email = 'foo@bar';
    const currentPassword = 'foobar';
    const newPassword = 'foobarnew';
    const message = 'Senha resetada com sucesso';
    const orsegupsId = app.get('orsegupsId');
    const endpoint = `${orsegupsId.uri}/user/changepassword`;
    const status = 200;
    axios.put = jest.fn(() => Promise.resolve({
      data: { message, data: null },
      status,
    }));
    const service = PasswordChangeService(app);
    const data = {
      email, status, currentPassword, newPassword,
    };
    const accessToken = 'foo';
    const params = {
      clientToken: { accessToken },
    };
    const result = await service.create(data, params);
    expect(axios.put).toBeCalledWith(
      endpoint,
      {
        email,
        currentPassword,
        newPassword,
      },
      {
        withCredentials: true,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `bearer ${accessToken}`,
        },
      },
    );
    expect(result).toMatchObject({
      id: email,
      message,
      status,
    });
  });
});

/* eslint-disable camelcase */
import axios from 'axios';

export default function PasswordResetService(app) {
  return {
    async create(data) {
      const orsegupsId = app.get('orsegupsId');
      const { access_token } = app.serverToken;
      const endpoint = `${orsegupsId.uri}/user/resetpassword?access_token=${access_token}`;
      const res = await axios.put(
        endpoint,
        { email: data.email },
        {
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      const reset = { id: data.email, message: res.data.message, status: res.status };
      return reset;
    },
  };
}

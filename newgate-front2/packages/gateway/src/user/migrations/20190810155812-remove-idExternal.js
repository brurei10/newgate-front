/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.removeColumn('Users', 'idExternal'),
  down: (QI, Sequelize) => QI.addColumn('Users', 'idExternal', {
    type: Sequelize.STRING,
  }),
};

/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'phone', {
    type: Sequelize.STRING,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'phone'),
};

/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'cpfCnpj', {
    type: Sequelize.STRING,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'cpfCnpj'),
};

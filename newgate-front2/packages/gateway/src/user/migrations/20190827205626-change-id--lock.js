/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'LOCK TABLES SequelizeMeta WRITE, Users WRITE, Accounts WRITE, Tokens WRITE;',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'UNLOCK TABLES;',
  ),
};

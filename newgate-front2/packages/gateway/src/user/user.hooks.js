import updateIfExists from '../lib/hooks/updateIfExists';
import inviteOrsIdUser from './inviteOrsIdUser';
import patchOrsIdUser from './patchOrsIdUser';
import passwordHash from './passwordHash.hook';

const inviteOrsIdUserHook = inviteOrsIdUser();

export default {
  before: {
    create: [
      inviteOrsIdUserHook.beforeCreate,
      updateIfExists([
        'id',
      ], true),
      passwordHash,
    ],
    patch: [
      patchOrsIdUser(),
      passwordHash,
    ],
    update: [
      passwordHash,
    ],
  },
  after: {
    create: [
      inviteOrsIdUserHook.afterCreate,
    ],
  },
};

/* istanbul ignore file */
/* eslint-disable camelcase */
import axios from 'axios';
import { logger } from '@/logger';
import OrsegupsIdError from '@/lib/OrsegupsIdError';

export default function PasswordChangeService(app) {
  return {
    async create(data, params) {
      logger.debug('changing password for %s on orsegups id', data.email);
      const orsegupsId = app.get('orsegupsId');
      const { accessToken } = params.clientToken;
      const endpoint = `${orsegupsId.uri}/user/changepassword`;
      try {
        const res = await axios.put(
          endpoint,
          {
            email: data.email,
            currentPassword: data.currentPassword,
            newPassword: data.newPassword,
          }, {
            withCredentials: true,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `bearer ${accessToken}`,
            },
          },
        );
        const change = { id: data.email, message: res.data.message, status: res.status };
        return change;
      } catch (err) {
        const idError = err.response && new OrsegupsIdError(err);
        logger.error(
          'error changing password for %s on orsegups id',
          data.email,
        );
        throw idError || err;
      }
    },
  };
}

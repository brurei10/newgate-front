/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
import request from 'request';
import path from 'path';
import fs from 'fs';

function getPath(file) {
  return path.resolve(__dirname, `../config/${file}.json`);
}

function readConfig(file) {
  return JSON.parse(fs.readFileSync(getPath(file)));
}

const NODE_ENV = process.env.NODE_ENV || 'default';

const config = readConfig(NODE_ENV);

const hostURL = `http://${config.host}:${config.port}`;

console.log('running job with config', config);

if (process.argv.includes('refreshToken')) {
  const url = `${hostURL}/jobs/refreshToken`;
  console.log('sending GET request to', url);
  request(url, (err, res) => {
    if (err || res.statusCode !== 200) {
      console.error('refreshToken', 'error', err || res.statusCode);
    }
  });
}

#!/bin/bash

DOM=$(basename $(dirname $(dirname $1)))
TNAME=$(basename $1)
FNAME=$(echo ${TNAME/.js/.$DOM.js})

ln -s $1 $2/$FNAME
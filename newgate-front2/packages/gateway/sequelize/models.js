const Sequelize = require('sequelize');
const app = require('../src/app');

const sequelize = app.get('sequelizeClient');
const { models } = sequelize;

module.exports = Object.assign({
  Sequelize,
  sequelize,
}, models);
